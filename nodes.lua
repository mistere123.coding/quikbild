local S = core.get_translator("quikbild")


core.register_node("quikbild:climb", {
    description = S("Quikbild Climb-able Node"),
    drawtype = "airlike",
    tiles = {},
    inventory_image = "quikbild_climb.png",
    wield_image = "quikbild_climb.png",
    pointable = false,
    buildable_to = true,
    climbable = true,
    walkable = false,
    sunlight_propagates = true,
    paramtype = 'light',
    light_source = 6,
    
})



quikbild.items = {}
local storage = quikbild.storage
local colors = {
    {"black", S("Black Block")},
    {"blue", S("Blue Block")},
    {"brown", S("Brown Block")},
    {"cyan", S("Cyan Block")},
    {"dark_green", S("Dark Green Block")},
    {"dark_grey", S("Dark Grey Block")},
    {"green", S("Green Block")},
    {"grey", S("Grey Block")},
    {"magenta", S("Magenta Block")},
    {"orange", S("Orange Block")},
    {"pink", S("Pink Block")},
    {"red", S("Red Block")},
    {"violet", S("Violet Block")},
    {"white", S("White Block")},
    {"yellow", S("Yellow Block")},
}

for i = 1, 15 do
    
	local name = colors[i][1]
	local desc = colors[i][2]

    
	core.register_node("quikbild:" .. name, {
		description = desc,
		tiles = {"quikbild_wool_" .. name .. ".png"},
        range = 10.0,
		is_ground_content = false,
        groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3,
				flammable = 3, wool = 1},
        on_place = function(itemstack, placer, pointed_thing)
            if placer:is_player() then
                local p_name = placer:get_player_name()
                if arena_lib.is_player_in_arena(p_name, "quikbild") then
                    local arena = arena_lib.get_arena_by_player(p_name)
                    --core.chat_send_all('ln17')
                    local pos = pointed_thing.above
                    if pos and core.get_node(pos).name == 'air' or string.find(core.get_node(pos).name,'quikbild') then  
                        core.set_node(pos, {name="quikbild:" .. name})
                        local poss = {}
                        local ser_poss = storage:get_string("pos_"..arena.name)
                        if ser_poss then
                            poss = core.deserialize(ser_poss)
                        end
                        table.insert(poss,pos)
                        storage:set_string("pos_"..arena.name,core.serialize(poss))
                        return ItemStack("quikbild:" .. name), pos
                    end
                end
            end

        end,
        drop = {},
        on_drop = function() return end,
        on_use = function(itemstack, user, pointed_thing)
            if arena_lib.is_player_in_arena(user:get_player_name(), "quikbild") then
                local pos = pointed_thing.under 
                if pos and string.find(core.get_node(pos).name,'quikbild') then  

                    core.set_node(pos, {name="quikbild:climb"})
                end
            end
            return nil
        
        end,
		
    })
    table.insert(quikbild.items,"quikbild:" .. name)
end

